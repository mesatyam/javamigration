package com.demo11.models;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Dto {
    private String name;
    private String phoneNumber;
}
