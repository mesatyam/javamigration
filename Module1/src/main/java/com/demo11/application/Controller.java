package com.demo11.application;

import com.demo11.models.Dto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/yo")
    public String hello(){
        return "hello yo";
    }
    @PostMapping("/user")
    public String hello(@RequestBody Dto dto){
        return dto.toString();
    }
}
